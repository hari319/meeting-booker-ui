# Project Description:-

- Portal for managing room bookings for meeting rooms
- Users need to log in to use the site.

  - If a subpage is called up without being logged in, you will be directed to the login page
  - Login status is managed via the context
  - Users are hardcoded with password in the frontend
  - 2 roles
    - Admin:
      - Can create and manage rooms
      - See all bookings for a room
    - User:
      - See his bookings, can cancel the booking again
      - Can book rooms by the hour
  - The site can be used completely mobile
  - The site can be used in German and English
  - A room cannot be booked twice
  - Page is based on Syngenio CI

  - Technical requirements:

  - React front end
    - Routing with React router
    - State administration with Redux or Context
    - Rect-intl for internationalization
  - Spring backend
    - Remaining interfaces
    - Data storage in an H2 DB

### `yarn install`

Install all the dependencies listed within package.json in the local node_modules folder.

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `Meeting Booker Backend`

- Backend must be started locally in order to utilize this app.
- Backend can be found in this [link](https://gitlab.com/hari319/webappbackend)

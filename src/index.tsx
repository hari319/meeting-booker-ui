import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import MainHeader from "./component/MainHeader";
import LanguageSelect from "./component/MutliLanguage/LanguageSelect";

const rootElement = document.getElementById("root");
ReactDOM.render(
  <React.StrictMode>
    <MainHeader />
    <div className={"Lang"}>
      <LanguageSelect />
    </div>
    <App />
  </React.StrictMode>,
  rootElement
);


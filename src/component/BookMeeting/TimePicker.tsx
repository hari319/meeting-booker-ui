import React, { FunctionComponent } from "react";
import { KeyboardTimePicker } from "@material-ui/pickers";
import { useTranslation } from "react-i18next";
import i18n from "i18next";

interface TimePickerProps {
  value?: any;
  label: string;
  onChange: (value: any) => void;
}

export const TimePicker: FunctionComponent<TimePickerProps> = ({
  value,
  onChange,
  label,
}) => {
  const { t } = useTranslation();
  return (
    <KeyboardTimePicker
      ampm={i18n.language === "en" ? true : false}
      variant="inline"
      margin="normal"
      id="time-picker"
      label={t(label)}
      value={value}
      onChange={(e) => {
        onChange(e);
      }}
      KeyboardButtonProps={{
        "aria-label": "change time",
      }}
    />
  );
};

export default TimePicker;

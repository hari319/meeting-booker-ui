import "date-fns";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import PageHeader from "../SelectItem/Header";
import { makeStyles } from "@material-ui/core/styles";
import DateTime from "./DateTime";
import MeetingTable from "./meetingTable";
import RoomSelection from "./RoomSelection";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: "10px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  row: {
    marginTop: theme.spacing(4),
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: "250px",
    paddingRight: "250px",
  },
}));

export default function Booking() {
  const classes = useStyles();
  return (
    <div>
      <Container component="main" maxWidth="lg">
        <CssBaseline />
        <div className={classes.paper}>
          <PageHeader id="BOOKING" />
        </div>
        <RoomSelection />
        <DateTime />
        <MeetingTable />
      </Container>
    </div>
  );
}

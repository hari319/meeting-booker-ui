/* eslint-disable @typescript-eslint/no-redeclare */
/* eslint-disable react-hooks/rules-of-hooks */
import React, { FunctionComponent, useState } from "react";
import {
  MenuItem,
  InputLabel,
  FormControl,
  useTheme,
  useMediaQuery,
} from "@material-ui/core";
import Select from "@material-ui/core/Select";
import { useTranslation } from "react-i18next";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { Context } from "../Context";

interface InputFieldProps {
  onChange: (type: any, id: any) => void;
  id: any;
}

const Dropdown: FunctionComponent<InputFieldProps> = ({ id, onChange }) => {
  const [value, setValue] = useState<string | number>("");
  const [open, setOpen] = useState(false);
  const { t } = useTranslation();
  const { loginRole } = React.useContext(Context);
  const theme = useTheme();
  const matchesMobile = useMediaQuery(theme.breakpoints.down("xs"));

  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      select: {
        width: "90%",
      },
      formControl: {
        width: matchesMobile ? "40%" : "60%",
      },
    })
  );

  const classes = useStyles();

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setValue(event.target.value as number);
    onChange(event.target.value, id);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };
  return (
    <>
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-controlled-open-select-label">
          {t("SELECT")}
        </InputLabel>
        <Select
          labelId="demo-controlled-open-select-label"
          id="demo-controlled-open-select"
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          value={value}
          onChange={handleChange}
          className={classes.select}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem
            value={"edit"}
            disabled={loginRole === "admin" ? false : true}
          >
            {t("EDIT")}
          </MenuItem>
          <MenuItem value={"remove"}>{t("REMOVE")}</MenuItem>
        </Select>
      </FormControl>
    </>
  );
};

export default Dropdown;

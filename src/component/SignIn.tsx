import React, { useState, useContext } from 'react';
import Avatar from '@material-ui/core/Avatar';
import CssBaseline from '@material-ui/core/CssBaseline';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import InputField from './SelectItem/InputField';
import Button from './SelectItem/Button';
import { Context } from './Context';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignIn() {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const { setUserName, setLoginRole } = useContext(Context);
  const history = useHistory();
  const { t } = useTranslation();

  function setContentByRole(role: string) {
    setUserName(email);
    let setRole = role === 'user' ? 'user' : 'admin';
    setLoginRole(setRole);
    localStorage.setItem('token', '12345');
    history.push('/booking');
  }

  const onSubmit = (event: any) => {
    event.preventDefault();
    if (email === 'user' && password === '') {
      setContentByRole('user');
    } else if (email === 'admin' && password === '') {
      setContentByRole('admin');
    } else {
      window.alert('Wrong Email or Password');
    }
  };

  const classes = useStyles();

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          {t('SIGNIN')}
        </Typography>
        <form className={classes.form} onSubmit={onSubmit}>
          <InputField label="USERNAME" id="email" onChange={setEmail} />
          <InputField
            label="PASSWORD"
            id="password"
            type="password"
            onChange={setPassword}
          />
          <Button label="SIGNIN" />
        </form>
      </div>
    </Container>
  );
}

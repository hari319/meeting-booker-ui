import React from "react";
import { AppBar, Toolbar } from "@material-ui/core";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import logo from "../imgs/logoSYN.png";

function MainHeader() {
  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      root: {
        flexGrow: 1,
      },
      menuButton: {
        marginRight: theme.spacing(2),
      },
      title: {
        flexGrow: 1,
      },
      toolbar: {
        backgroundColor: "#93F100",
      },
      pTag: {
        margin: "0 auto",
        display: "block",
      },
    })
  );

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar className={classes.toolbar}>
          <div className={classes.pTag}>
            <img style={{ height: 40 }} alt={"Syngenio"} src={logo} />
          </div>
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default MainHeader;

import React, { FunctionComponent } from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import Info from "@material-ui/icons/Info";
import ScheduleIcon from "@material-ui/icons/Schedule";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    H1: {
      textAlign: "center",
    },
  })
);

interface HeaderProps {
  id: string;
}

const Header: FunctionComponent<HeaderProps> = ({ id }) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <>
      <Avatar className={classes.avatar}>
        {id === "BOOKING" ? <ScheduleIcon /> : <Info />}
      </Avatar>
      <Typography component="h1" variant="h5" className={classes.H1}>
        {t(id)}
      </Typography>
    </>
  );
};

export default Header;

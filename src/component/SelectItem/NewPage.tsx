import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import PageHeader from "./Header";
import Title from "./Title";
import Card from "./Card";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    columm: {
      marginTop: theme.spacing(6),
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
  })
);

function NewPage() {
  const classes = useStyles();
  return (
    <Container maxWidth="lg">
      <CssBaseline />
      <div className={classes.columm}>
        <PageHeader id="FRUITS_WIKI" />
        <Title />
      </div>
      <Card />
    </Container>
  );
}

export default NewPage;

import React, { FunctionComponent } from "react";
import TextField from "@material-ui/core/TextField";
import { useTranslation } from "react-i18next";

interface InputFieldProps {
  label: string;
  value?: string;
  onChange: (value: string) => void;
  type?: string;
  id: string;
}

export const StringEditor: FunctionComponent<InputFieldProps> = ({
  label,
  value,
  type,
  id,
  onChange,
}) => {
  const { t } = useTranslation();
  return (
    <>
      <TextField
        className={"Input"}
        variant="outlined"
        // required
        margin="normal"
        fullWidth
        id={id}
        label={t(label)}
        type={type}
        defaultValue={value}
        onChange={(e: any) => {
          onChange(e.target.value);
        }}
      />
    </>
  );
};

export default StringEditor;
